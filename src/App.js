import React from 'react';
import CssBaseline from "@material-ui/core/CssBaseline";
import AboutDialog from './components/AboutDialog/AboutDialog';
import PiToolbar from './components/PiToolbar/PiToolbar';
import PiMap from './components/PiMap/PiMap';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/core/styles';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    content: {
        flexGrow: 1,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        color: 'black'
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
}));

const App = () => {
    const classes = useStyles();
    const [isDialogOpen, setIsDialogOpen] = React.useState(false);

    const darkTheme = createMuiTheme({
        palette: {
            type: 'dark',
        },
    });

    const handleDialogOpen = () => {
        setIsDialogOpen(true);
    };

    const handleDialogClose = () => {
        setIsDialogOpen(false);
    };

    return (
        <ThemeProvider theme={darkTheme}>
            <div className={classes.root}>
                <CssBaseline/>
                <PiToolbar handleDialogOpen={handleDialogOpen} />
                <main className={clsx(classes.content)}>
                    <PiMap/>
                </main>
                <AboutDialog isDialogOpen={isDialogOpen} onClose={handleDialogClose} />
            </div>
        </ThemeProvider>
    );

};

export default App;
