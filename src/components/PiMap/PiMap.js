import React, {useEffect, useState} from 'react';
import {Map, TileLayer, LayersControl} from 'react-leaflet';

const styles = {
    mapContainer: (desktop, landscape) => ({
        height: desktop ? 'calc(100vh - 64px)' : (landscape ? 'calc(100vh - 48px)' : 'calc(100vh - 56px)'),
        marginTop: desktop ? '64px' : (landscape ? '48px' : '56px'),
    })
};

export default function PiMap() {
    const zoom = 3;
    const center = {
        lat: -50,
        lng: 0,
    };
    const desktopMediaMatch = window.matchMedia('(min-width: 600px)');
    const landscapeMediaMatch = window.matchMedia('(min-width: 0px) and (orientation: landscape)');
    const [desktopMediaMatches, setDesktopMediaMatches] = useState(desktopMediaMatch.matches);
    const [landscapeMediaMatches, setLandscapeMediaMatches] = useState(desktopMediaMatch.matches);

    useEffect(() => {
        const desktopHandler = e => setDesktopMediaMatches(e.matches);
        const landscapeHandler = e => setLandscapeMediaMatches(e.matches);
        desktopMediaMatch.addListener(desktopHandler);
        landscapeMediaMatch.addListener(landscapeHandler);

        return () => {
            desktopMediaMatch.removeListener(desktopHandler);
            landscapeMediaMatch.removeListener(landscapeHandler);
        };
    });

    return (
        <Map center={center} zoom={zoom} style={styles.mapContainer(desktopMediaMatches, landscapeMediaMatches)} maxZoom={7}>
            <LayersControl position="topright">
                <LayersControl.BaseLayer checked name="Astro Pi Map">
                    <TileLayer
                        url="https://pardubice-pi.cz/imgExist.php?dir=new/{z}-{x}-{y}.png"/>
                </LayersControl.BaseLayer>
                <LayersControl.BaseLayer name="NDVI Map">
                    <TileLayer
                        url="https://pardubice-pi.cz/imgExist.php?dir=newNDVI/{z}-{x}-{y}.png"/>
                </LayersControl.BaseLayer>
            </LayersControl>
        </Map>
    );
};

