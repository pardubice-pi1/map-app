import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import PeopleIcon from '@material-ui/icons/People';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import {map} from 'ramda';

const dialogTitleStyles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    }
});

const styles = (theme) => ({
    link: {
        color: 'white'
    }
});

const DialogTitle = withStyles(dialogTitleStyles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h5">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);


const AboutDialog = ({ isDialogOpen, onClose, classes }) =>{
    return (
        <Dialog onClose={onClose} aria-labelledby="simple-dialog-title" open={isDialogOpen}>
            <DialogTitle onClose={onClose}>
                About
            </DialogTitle>
            <DialogContent dividers>
                <Typography variant='h6' gutterBottom>Pardubice Pi</Typography>
                <Typography gutterBottom>
                    We are Astro Pi competition team called Pardubice Pi and we are performing the Life on Earth mission.
                </Typography>
                <Typography variant='h6' gutterBottom>Team members</Typography>
                <List>
                    {map(name => (<ListItem>
                        <ListItemIcon>
                            <PersonIcon />
                        </ListItemIcon>
                        <ListItemText primary={name} />
                    </ListItem>), ['Václav Pavlíček', 'Filip Filipi', 'Patrik Kratochvíl', 'Tomáš Paulysko'])}
                </List>
                <Typography variant='h6' gutterBottom>Team mentor</Typography>
                <List>
                    <ListItem>
                        <ListItemIcon>
                            <PersonIcon />
                        </ListItemIcon>
                        <ListItemText primary={'Ing. Vladimíř Kašpar'} />
                    </ListItem>
                </List>
                <Typography gutterBottom>
                    To learn more about the Astro Pi competition, checkout <a href="https://astro-pi.org/" target="_blank" className={classes.link}>this link</a>.
                </Typography>
                <Typography gutterBottom>
                    To view report from our mission, checkout <a href="https://pardubice-pi.cz/res/report.pdf" target="_blank" className={classes.link}>this link</a>.
                </Typography>
                <Typography gutterBottom>
                    Did you participate in the Astro Pi competition and would you like to share your photos with us? Do not hesitate and contact us via email <a href="mailto:vaclav@pavlicek.cz" className={classes.link}>vaclav@pavlicek.cz</a>, we will be pleased to add your photos to our map.
                </Typography>
                <Typography variant='h6' gutterBottom>Special credits</Typography>
                <Typography gutterBottom>
                    We would like to thank all the other Astro Pi that shared their photos with us! Below is the list of them.
                </Typography>
                <List>
                    {map(name => (<ListItem>
                        <ListItemIcon>
                            <PeopleIcon />
                        </ListItemIcon>
                        <ListItemText primary={name} />
                    </ListItem>), ['42Diggers42', 'Error 404', 'CM_Pi'])}
                </List>
            </DialogContent>
        </Dialog>
    );
};

export default withStyles(styles)(AboutDialog);